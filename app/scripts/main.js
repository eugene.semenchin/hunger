$(document).ready(function () {
  initSelect('.people-select');
  logicMobileBtn();
});

function initSelect (select) {
  $(select).select2({
    minimumResultsForSearch: Infinity
  });
}

function logicMobileBtn () {
  $('.btn-menu').click(function (e) {
    e.preventDefault();
    $('.header-menu').slideToggle(300);
  })
}

$(document).ready(function(){
  $('#main, #button').on('click','a', function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
      top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
  });
});

//скролл//

// var $page = $('html, body');
// $('a[href*="#"]').click(function() {
//   $page.animate({
//     scrollTop: $($.attr(this, 'href')).offset().top
//   }, 400);
//   return false;
// });
